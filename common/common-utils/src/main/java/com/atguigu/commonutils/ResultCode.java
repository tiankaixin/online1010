package com.atguigu.commonutils;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月04日 19:55
 */
public interface ResultCode {
    public static Integer SUCCESS = 20000;

    public static Integer ERROR = 20001;
}
