package com.atguigu.oss.controller;

import com.atguigu.commonutils.R;
import com.atguigu.oss.service.Osservice;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月07日 14:59
 */
@RequestMapping("/eduoss/fileoss")
@RestController
public class OssController {

    @Autowired
    private Osservice osservice;

    //上传头像的方法
    @ApiOperation(value = "文件上传")
    @PostMapping("/upload")
    public R uploadOssFile(MultipartFile file){
        // 获取上传的文件
        //返回上传到oss的路径
        String url = osservice.uploadFileAvatar(file);
        return R.ok().data("url",url);
    }
}
