package com.atguigu.educenter.service.impl;

import com.atguigu.educenter.entity.UcenterMember;
import com.atguigu.educenter.entity.vo.RegisterVo;
import com.atguigu.educenter.mapper.UcenterMemberMapper;
import com.atguigu.educenter.service.UcenterMemberService;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import com.atguigu.utils.JwtUtils;
import com.atguigu.utils.MD5;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author atguigu
 * @since 2021-08-09
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {


    @Autowired
    private RedisTemplate<String, String> redisTemplate;


    //用户登录
    @Override
    public String login(UcenterMember member) {
        String mobile = member.getMobile();
        String password = member.getPassword();
        if(StringUtils.isEmpty(mobile) ||StringUtils.isEmpty(password)){
            throw new GuliException(20001,"登录失败");
        }

        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        UcenterMember mobileMember = baseMapper.selectOne(wrapper);
        if(mobileMember == null){
            //数据库中没有这个手机号
            throw new GuliException(20001,"手机号不存在");
        }

        //判断密码
        //存储在数据库中的密码加密了
        //把我们输入的密码进行加密 然后比较
        //加密方式  MD5
        if (!MD5.encrypt(password).equals(mobileMember.getPassword())) {
            throw new GuliException(20001, "密码错误");
        }

        //判断用户是否被禁用
        if (mobileMember.getIsDisabled()) {
            throw new GuliException(20001, "用户被禁用");
        }

        //登录成功
        String jwtToken = JwtUtils.getJwtToken(mobileMember.getId(), mobileMember.getNickname());
        return jwtToken;
    }

    //查询某一天注册人数
    @Override
    public int countRegisterDay(String day) {
        int count  = baseMapper.countRegisterDay(day);
        return count;
    }

    //用户注册
    @Override
    public void register(RegisterVo registerVo) {
        //获取用户数据
        System.out.println(registerVo);
        String code = registerVo.getCode();
        String mobile = registerVo.getMobile();
        String password = registerVo.getPassword();
        String nickname = registerVo.getNickname();
        //非空判断
        if(StringUtils.isEmpty(code) ||StringUtils.isEmpty(mobile)||StringUtils.isEmpty(password)||StringUtils.isEmpty(nickname)){
            throw  new GuliException(20001,"注册失败");
        }

        //判断验证码
        //获取redis验证码
        // String redisCode = redisTemplate.opsForValue().get(mobile);
        // String redisCode = redisTemplate.opsForValue().get("8888");
        // String redisCode = "8888";

        //暂时先不验证 验证码
        // if (!code.equals(redisCode)) {
        //     throw new GuliException(20001, "验证码不正确");
        // }

        //手机号不能够重复
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        Integer count = baseMapper.selectCount(wrapper);
        if(count>0){
            throw  new GuliException(20001,"手机号已经被注册");
        }
        UcenterMember member = new UcenterMember();
        member.setMobile(mobile);
        member.setPassword(MD5.encrypt(password));
        member.setNickname(nickname);
        member.setAvatar("http://thirdwx.qlogo.cn/mmopen/vi_32/zZfLXcetf2Rpsibq6HbPUWKgWSJHtha9y1XBeaqluPUs6BYicW1FJaVqj7U3ozHd3iaodGKJOvY2PvqYTuCKwpyfQ/132");
        baseMapper.insert(member);
    }

}
