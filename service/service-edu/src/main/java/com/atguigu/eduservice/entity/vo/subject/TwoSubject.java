package com.atguigu.eduservice.entity.vo.subject;

import lombok.Data;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月08日 11:43
 */
@Data
public class TwoSubject {
    private String id;
    private String title;
}
