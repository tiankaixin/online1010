package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.client.VodClient;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author atguigu
 * @since 2021-08-07
 */
@RestController
@RequestMapping("/eduservice/video")
public class EduVideoController {
    @Autowired
    private EduVideoService videoService;

    @Autowired
    private VodClient vodClient;
    //添加小节
    @PostMapping("addVedio")
    public R addVedio(@RequestBody EduVideo eduVideo){
        System.out.println("***********");
        System.out.println(eduVideo);
        System.out.println("***********");
        videoService.save(eduVideo);
        return R.ok();
    }

    //根据小节id删除小节(外加里面的视频内容)
    @DeleteMapping("deleteVedio/{id}")
    public R deleteVedio(@PathVariable String id){
        //根据小节id 获取视频id
        EduVideo eduVideo = videoService.getById(id);
        String videoSourceId = eduVideo.getVideoSourceId();
        //先删除阿里云里面的视频
        if(!StringUtils.isEmpty(videoSourceId)){
            R result = vodClient.removeAliyunVideo(videoSourceId);
            if(result.getCode() == 20001){
                throw  new GuliException(20001,"删除视频失败,熔断器......");
            }
        }
        //然后再删除数据库里面的相关内容
        videoService.removeById(id);
        return R.ok();
    }
}

