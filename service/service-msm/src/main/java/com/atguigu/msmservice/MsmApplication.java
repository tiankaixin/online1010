package com.atguigu.msmservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月09日 19:32
 */
@EnableFeignClients    //服务调用
@ComponentScan(basePackages = {"com.atguigu"})
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)

public class MsmApplication {
    public static void main(String[] args) {
        try {
            SpringApplication.run(MsmApplication.class,args);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
