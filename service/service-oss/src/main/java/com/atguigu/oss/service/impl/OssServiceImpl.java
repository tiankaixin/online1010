package com.atguigu.oss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.atguigu.oss.service.Osservice;
import com.atguigu.oss.utils.ConstantPropertiesUtil;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月07日 15:00
 */
@Service
public class OssServiceImpl implements Osservice {

    //上传文件到oss

    @Override
    public String uploadFileAvatar(MultipartFile file) {
        //1.获取阿里云存储相关常量
        //获取阿里云存储相关常量
        String endPoint = ConstantPropertiesUtil.END_POINT;
        String accessKeyId = ConstantPropertiesUtil.KEY_ID;
        String accessKeySecret = ConstantPropertiesUtil.KEY_SELECT;
        String bucketName = ConstantPropertiesUtil.BUCKET_NAME;
        try{
            //创建OSS实例
            OSS ossClient = new OSSClientBuilder().build(endPoint, accessKeyId, accessKeySecret);
            //获取上传文件流
            InputStream inputStream = file.getInputStream();
            //获取文件名
            String filename = file.getOriginalFilename();
            //1 在文件名称里面添加随机唯一的值
            String uuid = UUID.randomUUID().toString().replace("-", "");
            filename = uuid+filename;

            //2 把文件按照日期进行分类
            String datePath = new DateTime().toString("yyyy/MM/dd");
            filename = datePath+ "/" +filename;
            //调用oss方法实现上传
            ossClient.putObject(bucketName,filename,inputStream);
            //关闭OSSClient对象
            ossClient.shutdown();
            //把上传之后文件路径返回
            //需要把上传到阿里云OSS路径拼接出来
            String url =  "http://"+bucketName+"."+endPoint+"/"+filename;
            return url;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }
}
