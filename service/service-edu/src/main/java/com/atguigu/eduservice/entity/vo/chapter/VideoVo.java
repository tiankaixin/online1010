package com.atguigu.eduservice.entity.vo.chapter;

import lombok.Data;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月07日 20:30
 */
@Data
public class VideoVo {
    private String id;
    private String title;
    private String videoSourceId; //视频id
}
