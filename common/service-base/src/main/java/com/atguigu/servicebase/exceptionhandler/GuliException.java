package com.atguigu.servicebase.exceptionhandler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月04日 21:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuliException extends  RuntimeException{
    private Integer code;

    private String meg;
}
