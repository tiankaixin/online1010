package com.atguigu.staservice.schedule;

import com.atguigu.staservice.service.StatisticsDailyService;
import com.atguigu.staservice.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月11日 18:12
 */
@Component
public class ScheduleTask {

    @Autowired
    private StatisticsDailyService staService;

    @Scheduled(cron = "0 0 1 * * ?")
    public void task2(){
        //当前日期减1
        String date = DateUtil.formatDate(DateUtil.addDays(new Date(), -1));
        staService.registerCount(date);
        System.out.println("**************task1 执行了");
    }
}
