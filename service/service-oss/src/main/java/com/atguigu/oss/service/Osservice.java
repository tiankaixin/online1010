package com.atguigu.oss.service;

import org.springframework.web.multipart.MultipartFile;

public interface Osservice {

    String uploadFileAvatar(MultipartFile file);
}
