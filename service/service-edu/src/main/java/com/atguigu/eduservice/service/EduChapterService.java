package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduChapter;
import com.atguigu.eduservice.entity.vo.chapter.ChapterVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author atguigu
 * @since 2021-08-07
 */
public interface EduChapterService extends IService<EduChapter> {

    boolean deleteChapter(String chapterId);

    List<ChapterVo> getChapterVedioByCourseId(String courseId);

    void removeChapterByCourseId(String courseId);
}
