package com.atguigu.eduservice.client;

import com.atguigu.commonutils.R;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月08日 20:30
 */
@Component
public class VodFileDegradeFeignClient implements VodClient{
    @Override
    public R removeAliyunVideo(String videoId) {
        return R.error().message("删除视频出错了");
    }

    @Override
    public R deleteBatch(List<String> videoIdList) {
        return R.error().message("删除多个视频出错了");
    }
}
