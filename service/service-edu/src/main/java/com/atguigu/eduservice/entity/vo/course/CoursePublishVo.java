package com.atguigu.eduservice.entity.vo.course;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月07日 18:23
 */
//显示课程确认信息
@Data
public class CoursePublishVo {
    private String id;
    private String title;
    private String cover;
    private Integer lessonNum;
    private String subjectLevelOne;
    private String subjectLevelTwo;
    private String teacherName;
    private BigDecimal price;//只用于显示
}
