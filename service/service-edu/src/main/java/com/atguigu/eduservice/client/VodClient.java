package com.atguigu.eduservice.client;

import com.atguigu.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月08日 20:28
 */
@FeignClient(name = "service-vod",fallback =VodFileDegradeFeignClient.class )
@Component
public interface VodClient {
    //定义要调用方法的路径
    //@PathVariable注解一定要指定参数名称,否则出错
    @DeleteMapping("/eduvod/video/removeAliyunVideo/{videoId}")
    public R removeAliyunVideo(@PathVariable("videoId") String videoId);

    //删除多个阿里云视频的方法
    //参数是多个视频id
    @DeleteMapping("/eduvod/video/deleteBatch")
    public R deleteBatch(@RequestParam("videoIdList") List<String> videoIdList);
}
