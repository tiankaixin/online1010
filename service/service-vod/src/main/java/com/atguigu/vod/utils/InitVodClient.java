package com.atguigu.vod.utils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;

/**
 * @Description: TODO
 * @author: scott
 * @date: 2021年08月08日 15:40
 */
//相当于一个工具类
public class InitVodClient {

    public static DefaultAcsClient initVodClient(String accessKeyId,String accessKeySecret){
        String regionId = "cn-shanghai";  // 点播服务接入区域 不能修改
        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        return client;
    }
}
